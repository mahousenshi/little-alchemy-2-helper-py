from itertools import combinations_with_replacement
from colorama import Fore

import sqlite3

def dict_factory(cursor, row):
    return {col[0]: row[idx] for idx, col in enumerate(cursor.description)}

conn = sqlite3.connect('alchemy.db')
conn.row_factory = dict_factory
c = conn.cursor()

def percent(val, total):
    return f'{val/total*100:.2f}'

def add_combination(element1_id, element2_id, new_element_id):
    sql = '''
    INSERT INTO combinations(element1_id, element2_id, new_element_id)
    VALUES (?, ?, ?)
    '''

    c.execute(sql, (element1_id, element2_id, new_element_id))
    conn.commit()

def count_combinations():
    sql = '''
    SELECT COUNT(*) AS counter
    FROM combinations
    '''

    c.execute(sql)
    return c.fetchone()

def count_reactions():
    sql = '''
    SELECT COUNT(*) AS counter
    FROM combinations
    WHERE new_element_id <> 0
    '''

    c.execute(sql)
    return c.fetchone()

def fetch_elements():
    sql = '''
    SELECT *
    FROM elements
    WHERE depleted = 0
    ORDER BY name
    '''

    c.execute(sql)
    return c.fetchall()

def count_elements():
    sql = '''
    SELECT COUNT(*) AS counter
    FROM elements
    '''

    c.execute(sql)
    return c.fetchone()

def add_element(element):
    sql = '''
    INSERT INTO elements(name, depleted)
    VALUES (?, 0)
    '''

    c.execute(sql, (element,))
    conn.commit()

    return c.lastrowid

def depleted_element(name, id):
    sql = '''
    UPDATE elements
    SET depleted = 1
    WHERE id = ?
    '''

    c.execute(sql, (id,))
    conn.commit()

    sql = '''
    DELETE FROM combinations
    WHERE (element1_id = ? OR element2_id = ?)
    AND new_element_id = 0
    '''

    c.execute(sql, (id, id))

    lines = c.rowcount
    if lines:
        print(f'Removing {lines} no reaction{"s" if lines > 1 else ""} with {Fore.GREEN}{name}{Fore.RESET}.')

    conn.commit()

def search_element(name):
    sql = '''
    SELECT *
    FROM elements
    WHERE name LIKE ?
    '''

    c.execute(sql, (f'%{name}%',))
    return c.fetchall()

def string_list(elems):
    return [str(elem) for elem in range(len(elems))]

while True:
    elems = fetch_elements()

    combs = list(combinations_with_replacement([elem['id'] for elem in elems], 2))
    elements = {elem['id']: elem for elem in elems }

    if len(combs) == 0:
        quit()

    total = count_elements()['counter']
    alive = len(elements)
    depleted = total - alive

    print(f'\nTotal combinations: {count_combinations()["counter"]}')
    print(f'Total reactions: {count_reactions()["counter"]}')
    print(f'\nCombinations with non-depeleted elemnts: {len(combs)}')
    print(f'\nNumbers of depeleted elements: {depleted} ({percent(depleted, 720)}%)')
    print(f'Numbers of non-depeleted elements: {alive} ({percent(alive, 720)}%)')
    print(f'Numbers of elements: {total} ({percent(total, 720)}%)')

    f = 0
    for idy, comb in enumerate(combs):
        sql = '''
        SELECT *
        FROM combinations
        WHERE element1_id = ?
        AND element2_id = ?
        '''

        c.execute(sql, (min(comb), max(comb)))
        combination = c.fetchone()

        if combination is None:
            print(f'\nTry to combine: {Fore.RED}{elements[min(comb)]["name"]}{Fore.RESET} with {Fore.BLUE}{elements[max(comb)]["name"]}{Fore.RESET} ({idy+1})')

            reacted = input('\nIt reacted? ')
            if reacted:

                new_elem = ''
                if new_elem and total < 720:
                    elems = []

                    while True:
                        print("Element name:")
                        name = input('> ')

                        if not name:
                            break

                        new_element_id = add_element(name)

                        if not input('Basic element? '):
                            add_combination(min(comb), max(comb), new_element_id)

                        elems.append({
                            'id': new_element_id,
                            'name': name
                        })

                    for elem in elems:
                        depleted = input(f'Element {Fore.GREEN}{elem["name"]}{Fore.RESET} is depleted? ')
                        if depleted:
                            depleted_element(elem['name'], elem['id'])

                    f = 1

                old_element = 1
                if old_element:

                    while True:
                        print("Element name:")
                        name = input('> ')

                        if not name:
                            break

                        print()
                        elems = search_element(name)

                        print("\nChoose one:")
                        print(f'0> Reset')

                        if len(elems) == 1:
                            print(f'1> {Fore.GREEN}{elems[0]["name"]}{Fore.RESET}')
                            idx = 1

                        elif len(elems) > 1:
                            elems_str = string_list(elems + [''])

                            for n, elem in enumerate(elems, start=1):
                                print(f'{n}> {Fore.GREEN}{elem["name"]}{Fore.RESET}')

                            idx = 'a'
                            while idx not in elems_str:
                                idx = input('> ')

                            idx = int(idx)

                        if idx:
                            add_combination(min(comb), max(comb), elems[idx-1]['id'])

                depleted = input('\nCombined elements became depleted? ')
                if depleted:
                    idx = 3
                    if comb[0] == comb[1]:
                        idx = 1
                    else:
                        print(f'Wich')
                        print(f'0> {Fore.GREEN}Both{Fore.RESET} or 1> {Fore.RED}{elements[min(comb)]["name"]}{Fore.RESET} or 2> {Fore.BLUE}{elements[max(comb)]["name"]}{Fore.RESET}')

                    while idx not in [0, 1, 2]:
                        idx = int(input('> '))

                    if idx:
                        depleted_element(elements[min(comb) if idx == 1 else max(comb)]['name'], elements[min(comb) if idx == 1 else max(comb)]['id'])

                    else:
                        depleted_element(elements[min(comb)]['name'], elements[min(comb)]['id'])
                        depleted_element(elements[max(comb)]['name'], elements[max(comb)]['id'])

                    f = 1

            else:
                add_combination(min(comb), max(comb), 0)

        if f:
            break
