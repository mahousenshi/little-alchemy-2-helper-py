import sqlite3

def dict_factory(cursor, row):
    return {col[0]: row[idx] for idx, col in enumerate(cursor.description)}

conn = sqlite3.connect('alchemy.db')
conn.row_factory = dict_factory
c = conn.cursor()

sql = '''
SELECT *
FROM elements
WHERE depleted = 1
ORDER BY name
'''

c.execute(sql)

for elem in c.fetchall():
    sql = '''
    DELETE FROM combinations
    WHERE (element1_id = ? OR element2_id = ?)
    AND new_element_id = 0
    '''

    c.execute(sql, (elem['id'], elem['id']))
    n = c.rowcount
    conn.commit()

    if n:
        print(f"{elem['name']}\n{n}")

c.close()
conn.close()
